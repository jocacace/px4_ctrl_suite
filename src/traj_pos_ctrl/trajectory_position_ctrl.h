/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2018, Jonathan Cacace
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of SRI International nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#include "ros/ros.h"

#include "boost/thread.hpp"
#include <tf/transform_broadcaster.h>
#include "TooN/TooN.h"

//---Ros message headers
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/TwistStamped.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/Vector3.h>
//Socket functions
#include <fcntl.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/mman.h>
#include <sys/io.h>
#include <sys/time.h>
#include <netdb.h>
//---

//---Mavros specific command
#include <mavros_msgs/CommandBool.h>
#include <mavros_msgs/CommandTOL.h>
#include <mavros_msgs/WaypointPush.h>
#include <mavros_msgs/CommandCode.h>
#include <mavros_msgs/SetMode.h>
#include <mavros_msgs/PositionTarget.h>
#include <mavros_msgs/GlobalPositionTarget.h>
#include <mavros_msgs/State.h>
#include <std_msgs/Float32MultiArray.h>
//---



using namespace TooN;

typedef struct CMD {
	float x;
	float y;
	float z;
	float yaw;
}CMD;

class traj_pos_ctrl {
  public:
    traj_pos_ctrl();
    void run();
    void local_pose_cb( geometry_msgs::PoseStamped pose );
    void mavros_state_cb( mavros_msgs::State state );
		void cmd_p_cb( geometry_msgs::Point p );
		void cmd_o_cb( geometry_msgs::Vector3 o );
    void ref_pose();
    void local_target_input();
    void debug();
		void get_input_cmd();

  private:
    ros::NodeHandle _nh;
    ros::Subscriber _local_pose_sub;
    ros::Subscriber _mav_state_sub;
		ros::Subscriber _cmd_p_sub;
		ros::Subscriber _cmd_o_sub;

    ros::Publisher _target_pub;
    ros::Publisher _debug_pub;
    Vector<3> _local_pos;
    float _local_yaw;
    bool _first_local_pos;

    Vector<3> _ref_p;
    Vector<3> _ref_dp;
    Vector<3> _ref_ddp;
    float _ref_yaw;
		float _cmd_yaw;

    Vector<3> _cmd_p;		
    Vector<3> _ep;

    //---Paramters
    double _ref_omega0_xyz;
    double _ref_zita;
    double _ref_acc_max;
    double _ref_vel_max;
    //---

    bool _offboard_mode_on;
    std_msgs::Float32MultiArray _debug_array;

		CMD _input_cmd;


};
