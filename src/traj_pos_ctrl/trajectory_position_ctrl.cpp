/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2018, Jonathan Cacace
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of SRI International nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#include "trajectory_position_ctrl.h"

using namespace std;
using namespace TooN;


//Creazione socket in LETTURA
bool listener_socket(int port_number, int *sock) {

  sockaddr_in si_me;

  if ( (*sock=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1) {
    std::cout << "Listener::Open: error during socket creation!" << std::endl;
    return false;
  }

  memset((char *) &si_me, 0, sizeof(si_me));

  /* allow connections to any address port */
  si_me.sin_family = AF_INET;
  si_me.sin_port = htons(port_number);
  si_me.sin_addr.s_addr = htonl(INADDR_ANY);
  int bind_ok = bind(*sock, (struct sockaddr*)&si_me, sizeof(si_me));

  if ( bind_ok == -1 )
    return false;
  else
    return true;

}


/** @brief Converts Quaternion into a Rotation matrix
*
*  @param Quat Input quaternion (w, x, y, z)
*  @return Converted Matrix
*/
Matrix<3> QuatToMat(TooN::Vector<4> Quat){
	Matrix<3> Rot;
	double s = Quat[0];
	double x = Quat[1];
	double y = Quat[2];
	double z = Quat[3];
	Fill(Rot) = 1-2*(y*y+z*z),2*(x*y-s*z),2*(x*z+s*y),
	2*(x*y+s*z),1-2*(x*x+z*z),2*(y*z-s*x),
	2*(x*z-s*y),2*(y*z+s*x),1-2*(x*x+y*y);
	return Rot;
}

float YawFromQuat( TooN::Vector<4> quat ) {
  Matrix<3> R = QuatToMat( quat );
  return atan2(R[1][0],R[0][0]);
}

/** @brief Converts a Rotation Matrix into a Quaternion
*
*  @param Rot Input Matrix
*  @return Resulting quaternion
*/
TooN::Vector<4> MatToQuat(Matrix<3> Rot) {
	TooN::Vector<4> Quat;
	double tr = Rot[0][0]+ Rot[1][1]+ Rot[2][2];
	int ii;
	ii=0;
	if (Rot[1][1] > Rot[0][0]) ii=1;
	if (Rot[2][2] > Rot[ii][ii]) ii=2;
	double s;
	if (tr >= 0){
		s = sqrt((tr + 1));
		Quat[0] = s * 0.5;
		s = 0.5 / s;
		Quat[1] = (Rot[2][1] - Rot[1][2]) * s;
		Quat[2] = (Rot[0][2] - Rot[2][0]) * s;
		Quat[3] = (Rot[1][0] - Rot[0][1]) * s;
	}
	else {
		switch(ii) {
			case 0:
				s = sqrt((Rot[0][0]-Rot[1][1]-Rot[2][2]+1));
				Quat[1] = s * 0.5;
				s = 0.5 / s;
				Quat[2] = (Rot[1][0] + Rot[0][1]) * s;//Update pose estimation
				Quat[3] = (Rot[2][0] + Rot[0][2]) * s;
				Quat[0] = (Rot[2][1] - Rot[1][2]) * s;
			break;
			case 1:
				s = sqrt((Rot[1][1]-Rot[2][2]-Rot[0][0]+1));
				Quat[2] = s * 0.5;
				s = 0.5 / s;

				Quat[3] = (Rot[2][1] + Rot[1][2]) * s;
				Quat[1] = (Rot[0][1] + Rot[1][0]) * s;
				Quat[0] = (Rot[0][2] - Rot[2][0]) * s;
			break;
			case 2:
				s = sqrt((Rot[2][2]-Rot[0][0]-Rot[1][1]+1));
				Quat[3] = s * 0.5;
				s = 0.5 / s;
				Quat[1] = (Rot[0][2] + Rot[2][0]) * s;
				Quat[2] = (Rot[1][2] + Rot[2][1]) * s;
				Quat[0] = (Rot[1][0] - Rot[0][1]) * s;
			break;
		}
	}

	return Quat;
}

//---Get parameters
void load_param( double & p, double def, string name ) {
	ros::NodeHandle n_param("~");
	if( !n_param.getParam( name, p))
		p = def;
	cout << name << ": " << "\t" << p << endl;
}

void load_param( int & p, int def, string name ) {
	ros::NodeHandle n_param("~");
	if( !n_param.getParam( name, p))
		p = def;
	cout << name << ": " << "\t" << p << endl;
}

void load_param( bool & p, bool def, string name ) {
	ros::NodeHandle n_param("~");
	if( !n_param.getParam( name, p))
		p = def;
	cout << name << ": " << "\t" << p << endl;
}

void load_param( string & p, string def, string name ) {
	ros::NodeHandle n_param("~");
	if( !n_param.getParam( name, p))
		p = def;
	cout << name << ": " << "\t" << p << endl;
}
//---


void traj_pos_ctrl::get_input_cmd() {

	int cmd_socket;
  listener_socket( 9010, &cmd_socket );
	int slen;
	int rlen;
  sockaddr_in si_me, si_other;
   
	while( ros::ok() ) {

     rlen = recvfrom( cmd_socket, &_input_cmd, sizeof(_input_cmd), 0, (struct sockaddr*)&si_other, (socklen_t*)&slen);


		_cmd_p = makeVector( _input_cmd.x, _input_cmd.y, _input_cmd.z );
		_cmd_yaw = _input_cmd.z; 



	}


}

traj_pos_ctrl::traj_pos_ctrl() {

  _local_pose_sub = _nh.subscribe("/mavros/local_position/pose", 0, &traj_pos_ctrl::local_pose_cb, this);
  _mav_state_sub = _nh.subscribe("/mavros/state", 0, &traj_pos_ctrl::mavros_state_cb, this);
	_cmd_p_sub = _nh.subscribe("/cmd/position", 0, &traj_pos_ctrl::cmd_p_cb, this);
	_cmd_o_sub = _nh.subscribe("/cmd/orientation", 0, &traj_pos_ctrl::cmd_o_cb, this);
  _target_pub = _nh.advertise<mavros_msgs::PositionTarget>("/mavros/setpoint_raw/local", 0);
  _debug_pub = _nh.advertise<std_msgs::Float32MultiArray>("/debug", 0);


  load_param(_ref_omega0_xyz, 1.0, "ref_omega0_xyz" );
  load_param(_ref_zita, 0.5, "ref_zita" );
  load_param(_ref_acc_max, 0.3, "ref_acc_max" );
  load_param(_ref_vel_max, 0.5, "ref_vel_max" );

  _local_pos = Zeros;
  _local_yaw = 0.0;
  _first_local_pos = false;
  _offboard_mode_on = false;
}



void traj_pos_ctrl::mavros_state_cb( mavros_msgs::State state ) {
  if( state.mode == "OFFBOARD" && state.armed) {
    _offboard_mode_on = true;
  }
  else _offboard_mode_on = false;

}

void traj_pos_ctrl::cmd_p_cb( geometry_msgs::Point p ) {
	_cmd_p = makeVector(p.x, p.y, p.z );
}

void traj_pos_ctrl::cmd_o_cb( geometry_msgs::Vector3 o ) {
	_cmd_yaw = o.z;
}

void traj_pos_ctrl::local_pose_cb( geometry_msgs::PoseStamped pose ) {
  _local_pos = makeVector(pose.pose.position.x, pose.pose.position.y, pose.pose.position.z);
  _local_yaw = YawFromQuat( makeVector(pose.pose.orientation.w, pose.pose.orientation.x, pose.pose.orientation.y, pose.pose.orientation.z ));
  _first_local_pos = true;
}

void traj_pos_ctrl::ref_pose() {
  ros::Rate r(10);
  double ref_T = 1/10.0;

  mavros_msgs::PositionTarget ptarget;
  ptarget.coordinate_frame = mavros_msgs::PositionTarget::FRAME_BODY_NED;
  ptarget.type_mask =
    //mavros_msgs::PositionTarget::IGNORE_VX |
    //mavros_msgs::PositionTarget::IGNORE_VY |
    //mavros_msgs::PositionTarget::IGNORE_VZ |
    //mavros_msgs::PositionTarget::IGNORE_AFX |
    //mavros_msgs::PositionTarget::IGNORE_AFY |
    mavros_msgs::PositionTarget::IGNORE_AFZ |
    mavros_msgs::PositionTarget::FORCE |
    //mavros_msgs::PositionTarget::IGNORE_YAW |
    mavros_msgs::PositionTarget::IGNORE_YAW_RATE;

  while( !_first_local_pos )
    usleep(0.1*1e6);

  ROS_INFO("First local pose arrived!");

  //TODO!! Inizializzazione riferimento di posizione (x,y,z) dal last command (todo)
  _ref_p = _local_pos;
  _ref_yaw = _local_yaw;
  _cmd_p = _local_pos;
  //_ref_yaw = cmd_yaw;
  _ref_dp = Zeros;
  _ref_ddp = Zeros;
  //ref_dyaw = 0.0;
  //ref_ddyaw = 0.0;
  _ep = Zeros;

  while (ros::ok()) {

    if( !_offboard_mode_on ) {
      _ref_p = _local_pos;
			_ref_yaw = _local_yaw;
			_cmd_yaw = _local_yaw;
      _cmd_p = _local_pos;
      _ep = Zeros;
    }
    else {

      Vector<3> ddp = Zeros;
      Vector<3> dp = Zeros;

			_ref_yaw = _cmd_yaw;
      //Errore di posizione
      _ep = _cmd_p - _ref_p;


      ddp[0] = _ref_omega0_xyz * _ref_omega0_xyz * _ep[0] - 2.0 * _ref_zita * _ref_omega0_xyz * _ref_dp[0];
      ddp[1] = _ref_omega0_xyz * _ref_omega0_xyz * _ep[1] - 2.0 * _ref_zita * _ref_omega0_xyz * _ref_dp[1];
      ddp[2] = _ref_omega0_xyz  * _ref_omega0_xyz  * _ep[2] - 2.0 * _ref_zita * _ref_omega0_xyz * _ref_dp[2];

      //Aggiornamento accelerazione
      Vector<3> jerk = (ddp - _ref_ddp)/ref_T;
      double n_jerk = norm(jerk);

      if( n_jerk > 7.5) {
          jerk *= (7.5/n_jerk);
      }
      ddp = _ref_ddp + jerk*ref_T;

      double sa = 1.0;
      double n_acc = norm(ddp);
      if(n_acc > _ref_acc_max) {
          sa = _ref_acc_max/n_acc ;
      }
      _ref_ddp = ddp * sa;

      //Calcolo velocita' presunta
      dp = _ref_dp + _ref_ddp * ref_T;
      double n_vel = norm(dp);

      if(n_vel > _ref_vel_max ) {
          for(int i = 0; i<3; i++) {
              if(_ref_dp[i] * _ep[i] > 0) {
                  _ref_ddp[i] = 0.0;
              }
          }
          dp = _ref_dp + _ref_ddp*ref_T;
          _ref_dp = dp / (norm(dp) / _ref_vel_max);
      }
      else {
          //Aggiornamento velocita'
          _ref_dp += _ref_ddp * ref_T ;
      }
      //Aggiornamento posizione
      _ref_p  += _ref_dp*ref_T;
    }

    ptarget.header.stamp = ros::Time::now();
    ptarget.position.x = _ref_p[0];
    ptarget.position.y = _ref_p[1];
    ptarget.position.z = _ref_p[2];
    ptarget.velocity.x = _ref_dp[0];
    ptarget.velocity.y = _ref_dp[1];
    ptarget.velocity.z = _ref_dp[2];
    ptarget.acceleration_or_force.x = _ref_ddp[0];
    ptarget.acceleration_or_force.y = _ref_ddp[1];
		ptarget.yaw = _ref_yaw;
    _target_pub.publish( ptarget );
    r.sleep();
  }
}

void traj_pos_ctrl::local_target_input() {


  /* the leading space before the %c ignores space characters in the input */
  float desx, desy, desz;
  while (ros::ok()) {
    printf("Please enter a cartesian point in ENU reference froma [x y z]\n");
    scanf("%f %f %f", &desx, &desy, &desz );
    _cmd_p = makeVector(desx, desy, desz);
  }
}

void traj_pos_ctrl::run() {
  //des thread
  boost::thread ref_pose_t( &traj_pos_ctrl::ref_pose, this );
  boost::thread debug_t( &traj_pos_ctrl::debug, this );
  boost::thread local_target_input_t( &traj_pos_ctrl::local_target_input, this);

  ros::spin();
}

void traj_pos_ctrl::debug() {
  ros::Rate r(10);

  Vector<3> absolute_ep = Zeros;
  float absolute_e_yaw = 0.0;
  _debug_array.data.resize(9);

  while(ros::ok()) {

    _debug_array.data[0] = _ep[0];
    _debug_array.data[1] = _ep[1];
    _debug_array.data[2] = _ep[2];
    _debug_array.data[3] = norm(_ep);

    absolute_ep[0] = _ref_p[0] - _local_pos[0];
    absolute_ep[1] = _ref_p[1] - _local_pos[1];
    absolute_ep[2] = _ref_p[2] - _local_pos[2];

    _debug_array.data[4] = absolute_ep[0];
    _debug_array.data[5] = absolute_ep[1];
    _debug_array.data[6] = absolute_ep[2];
    _debug_array.data[7] = norm(absolute_ep);

    absolute_e_yaw = fabs( _local_yaw - _ref_yaw );

    _debug_array.data[8] = absolute_e_yaw;
    _debug_pub.publish( _debug_array );

    r.sleep();

  }
}

int main( int argc, char** argv) {
  ros::init( argc, argv, "trajectory_position_ctrl");
  traj_pos_ctrl tpc;
  tpc.run();

  return 1;
}
