#include "ros/ros.h"
#include "boost/thread.hpp"
#include "geometry_msgs/PoseStamped.h"
//---mavros_msgs
#include "mavros_msgs/State.h"
#include "mavros_msgs/CommandBool.h"
#include "mavros_msgs/SetMode.h"
#include "mavros_msgs/CommandTOL.h"
//---

using namespace std;

class px4_example {
  public:
    px4_example();
    void run();
    void mavros_state_cb( mavros_msgs::State mstate);
    void ctrl_loop();

  private:
    ros::NodeHandle _nh;
    ros::Subscriber _mavros_state_sub;
    ros::ServiceClient _arming_client;
    ros::ServiceClient _set_mode_client;
    ros::ServiceClient _land_client;
    ros::Publisher _local_pos_pub;
    mavros_msgs::State _mstate;
};

px4_example::px4_example() {
  _arming_client = _nh.serviceClient<mavros_msgs::CommandBool>("mavros/cmd/arming");
  _set_mode_client = _nh.serviceClient<mavros_msgs::SetMode>("mavros/set_mode");
  _local_pos_pub = _nh.advertise<geometry_msgs::PoseStamped>("mavros/setpoint_position/local", 0);
  _land_client = _nh.serviceClient<mavros_msgs::CommandTOL>("/mavros/cmd/land");
  _mavros_state_sub = _nh.subscribe( "/mavros/state", 0, &px4_example::mavros_state_cb, this);
}

void px4_example::mavros_state_cb( mavros_msgs::State mstate) {
  _mstate = mstate;
  cout << "Stato: " << _mstate << endl;
}


void px4_example::ctrl_loop() {

  mavros_msgs::CommandBool arm_cmd;
  arm_cmd.request.value = true;

  mavros_msgs::SetMode offb_set_mode;
  offb_set_mode.request.custom_mode = "OFFBOARD";

  if( _set_mode_client.call(offb_set_mode) && offb_set_mode.response.mode_sent){
    ROS_INFO("Manual mode enabled");
  }

  if( _arming_client.call(arm_cmd) && arm_cmd.response.success){
  }



  while(!_mstate.armed ) usleep(0.1*1e6);
  ROS_INFO("Vehicle armed");

  if( _set_mode_client.call(offb_set_mode) && offb_set_mode.response.mode_sent){
    ROS_INFO("Manual mode enabled");
  }


  geometry_msgs::PoseStamped pose;
  pose.pose.position.x = 0;
  pose.pose.position.y = 0;
  pose.pose.position.z = 2;

  //send a few setpoints before starting
  ros::Rate r(10);
  int i=0;
  while( ros::ok() &&  i++ < 20 ) {
    _local_pos_pub.publish(pose);
    r.sleep();
    cout << "i: "<< i << endl;
  }


  mavros_msgs::CommandTOL land_srv;
  _land_client.call( land_srv );

}


void px4_example::run() {
  boost::thread ctrl_loop_t(&px4_example::ctrl_loop, this);
  ros::spin();
}

int main(int argc, char** argv ) {
  ros::init(argc, argv, "px4_ctrl_example");
  px4_example p4e;
  p4e.run();

  return 0;
}
